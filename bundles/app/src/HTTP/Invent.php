<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 13.03.2017
 * Time: 15:43
 */
namespace Project\App\HTTP;

use PHPixie\HTTP\Request;
use PHPixie\Validate\Form;

class Invent extends Processor
{

    public function defaultAction($request)
    {
        $container = $this->components()->template()->get('app:list');

        $cameras = $this->components()->orm()
            ->query('camera')
            ->where('deleted', "FALSE")
            ->find();
        $container->cameras = $cameras;
        $jsonCam = "";
        foreach ($cameras as $cam) {
            $jsonCam[$cam->name] = $cam->urlAdmin;
        }
        $container->jsonCam = $jsonCam;
        $container->delete = FALSE;
        $container->areas = $this->components()->orm()->query('area')->find();
        $container->statuses = $this->components()->orm()->query('status')->find();
        $container->models = $this->components()->orm()->query('model')->find();
        $container->user = $this->ADuser;
        return $container;
    }

    public function historyAction($request)
    {
        $container = $this->components()->template()->get('app:list');
        $container->cameras = $this->components()->orm()
            ->query('historyCamera')
//            ->where('delete',"FALSE")
            ->find();
        $container->jsonCam = "";
        $container->delete = TRUE;
        $container->areas = $this->components()->orm()->query('area')->find();
        $container->models = $this->components()->orm()->query('model')->find();
        $container->statuses = $this->components()->orm()->query('status')->find();
        $container->user = $this->ADuser;
        return $container;
    }

    public function deleteAction($request)
    {

        $id = preg_replace('~\D+~', '', $request->attributes()->get('id'));
        $camera = $this->components()->orm()->query('camera')->in($id)->findOne();
        $camera->deleted = TRUE;
        $camera->save();
//        return $this->redirect('app.processor', ['processor' => 'invent']);
        return $this->redirect('app.frontpage');
    }

    public function newAction($request)
    {

        $query = $request->data();
        $orm = $this->components()->orm();
        $status = $orm->query('status')->where('name', $query->get('status'))->find();
        $area = $orm->query('area')->where('name', $query->get('area'))->find();
        $model = $orm->query('model')->where('name', $query->get('model'))->find();


        if (count($status->asArray(true, 'id')) > 0) {
            $status = $orm->query('status')->where('name', $query->get('status'))->findOne();
        } elseif ($query->get('status')) {
            $status = $orm->createEntity('status', [
                'name' => $query->get('status')
            ])->save();
        } else {
            return "error: empty status";
            //$status->id = '';
        }

        if (count($area->asArray(true, 'id')) > 0) {
            $area = $orm->query('area')->where('name', $query->get('area'))->findOne();
        } elseif ($query->get('area')) {
            $area = $orm->createEntity('area', [
                'name' => $query->get('area')
            ])->save();
        } else {
//            return "error: empty area";
            $area->id = '';
        }

        if (count($model->asArray(true, 'id')) > 0) {
            $model = $orm->query('model')->where('name', $query->get('model'))->findOne();
        } elseif ($query->get('model')) {
            $model = $orm->createEntity('model', [
                'name' => $query->get('model')
            ])->save();
        } else {
//            return "error: empty model";
            $model->id = 0;
        }

        $camArray = [
            'name' => $query->get('name'),
            'place' => $query->get('place'),
            'ipLocal' => $query->get('ipLocal'),
            'urlAdmin' => $query->get('urlAdmin'),
            'areaId' => $area->id,
            'statusId' => $status->id,
            'modelId' => $model->id,
            'comment' => $query->get('comment'),
            'login' => $query->get('login'),
            'password' => $query->get('password'),
            'userUpdate' => $_SERVER['AUTHENTICATE_SAMACCOUNTNAME'],
        ];
//        print_r($camArray);
//        echo $orm->query('model')->in($camArray['modelId'])->findOne()->name;
//        return "";
        if ($query->get('id')) {
            $camArray['id'] = $query->get('id');
            $cam = $orm->query('camera')->in($camArray['id'])->update($camArray);
//            $cam->update($camArray);
        } else {
            $orm->createEntity('camera', $camArray)->save();
        }
        unset($camArray['id']);
        $orm->createEntity('historyCamera', $camArray)->save();


        return $this->redirect('app.frontpage');

    }
}