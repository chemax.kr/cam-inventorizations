<?php

return [
    'relationships' => [

        // Each user may have multiple messages
//        [
//            'type'  => 'oneToMany',
//            'owner' => 'user',
//            'items' => 'message'
//        ],
        [
            'type'  => 'oneToMany',
            'owner' => 'area',
            'items' => 'camera'
        ],
        [
            'type'  => 'oneToMany',
            'owner' => 'model',
            'items' => 'camera'
        ],
        [
            'type'  => 'oneToMany',
            'owner' => 'status',
            'items' => 'camera'
        ],
        [
            'type'  => 'oneToMany',
            'owner' => 'area',
            'items' => 'historyCamera'
        ],
        [
            'type'  => 'oneToMany',
            'owner' => 'model',
            'items' => 'historyCamera'
        ],
        [
            'type'  => 'oneToMany',
            'owner' => 'status',
            'items' => 'historyCamera'
        ],
    ]
];