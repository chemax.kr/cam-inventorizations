<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Let's try Bootstrap 4 -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="stylesheet" href="/bundles/app/bootstrap.min.css">
    <script src="/bundles/app/list.min.js"></script>
<!--    <script src="http://listjs.com/assets/javascripts/list.min.js"></script>-->
	<!-- Include our own css -->
	<link rel="stylesheet" href="/bundles/app/main.css">

<!--    <link rel="stylesheet" href="/bundles/app/tablesorter.css">-->

	<title><?=$_($this->get('pageTitle', 'Камеры'))?></title>

</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
	<div class="container">
		<a class="navbar-brand  mr-auto" href="<?=$this->httpPath('app.frontpage')?>">Камеры</a>
        <a class="navbar-brand  mr-auto" href="<?=$this->httpPath('app.history')?>">История</a>
        <input type="submit" onclick="edit(this)" value="Добавить" class="btn navbar-brand  mr-auto">

		<?php if($user): ?>
			<!-- If user is loged in display a welcome message and a Sign Out button -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<b>Hi, <?=$_($user)?></b>
				</li>
			</ul>
		<?php endif;?>
	</div>
</nav>


<?php $this->childContent(); ?>


<!-- Bootstrap dependencies -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>

<!-- our own script file -->
<!--<script src="/bundles/app/jquery.tablesorter.js"></script>-->


<!-- Allow subtemplates to append their own scripts -->
<?=$this->block('scripts')?>
</body>
</html>