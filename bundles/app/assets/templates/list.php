<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 14.03.2017
 * Time: 10:00
 */
$this->layout('app:layout');
include_once($this->resolve('app:new'));

$i = 0;
?>


    <div id="cameras">
        <input class="search" placeholder="Search"/>

        <table border="1" id="myTable">
            <thead>
            <tr align="center">
                <th>
                    <a class="sort" data-sort="id">
                        ID
                    </a>
                </th>
                <th>
                    <a class="sort" data-sort="name">
                        Имя
                    </a>
                </th>
                <th>Скриншот</th>
                <th>
                    <a class="sort" data-sort="place">
                        Расположение
                    </a>
                </th>
                <th>
                    <a class="sort" data-sort="ip">
                        Локальный IP
                    </a>
                </th>
                <th>URL админки</th>
                <th>
                    <a class="sort" data-sort="location">
                        Участок
                    </a>
                </th>
                <th>
                    <a class="sort" data-sort="model">
                        Модель
                    </a>
                </th>
                <th>
                    <a class="sort" data-sort="status">
                    Состояние
                    </a>
                </th>
                <th>Последнее обновление</th>
                <th>Комментарий</th>
                <th>Логин</th>
                <th>Пароль</th>
                <?php if ($delete): ?>
                    <th>Удалено</th>
                <?php endif; ?>
                <th>Отредактировал</th>
                <?php if (!$delete): ?>
                    <th>Кнопки</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody class="list">
            <?php
            function checkCamArea($camera)
            {
                if ($camera->area())
                {
                    return $camera->area()->name;
                }
                else{ return "";}
            }
            ?>
            <?php foreach ($cameras as $camera): ?>

                <tr name="camera<?= $camera->id ?>" id="camera<?= $camera->id ?>">
                    <td name="id" class="id"><?= $camera->id ?></td>
                    <td name="name" class="name"><?= $_($camera->name) ?></td>
                    <td name="screenshot">
                        <p>
                            <img class="zoom" src="http://192.168.22.239/thumb/<?= $camera->name ?>.jpg" width="60">
                        </p>
                    </td>
                    <td name="place" class="place"><?= $camera->place ?></td>
                    <td name="ipLocal" class="ip"><?= $camera->ipLocal ?></td>
                    <td name="urlAdmin"><a name="urlAdmin" href="<?= $camera->urlAdmin ?>"
                                           target="_blank"><?= $camera->place ?></a></td>
                    <td name="area" class="location"><?=checkCamArea($camera)?></td>
                    <td name="model" class="model"><?php if ($camera->model()) {
                            echo $camera->model()->name;
                        } ?></td>
                    <?php
                    $status = $camera->status()->name;
                    $statusClr = "";
                    if (strtolower($status) == "в работе") {
                        $statusClr = "green";
                    } elseif (strtolower($status) == "в ремонте") {
                        $statusClr = "red";
                    }

                    ?>
                    <td name="status" class="status" style="color:<?= $statusClr ?>!important"><?= $status ?></td>
                    <td><?= $camera->updateDate ?></td>
                    <td name="comment"><?= $camera->comment ?></td>
                    <td name="login"><?= $camera->login ?></td>
                    <td name="password"><?= $camera->password ?></td>
                    <?php if ($delete): ?>
                        <td><?= ($camera->deleted) ? 'Да' : 'Нет' ?></td>
                    <?php endif; ?>
                    <td><?= $camera->userUpdate ?></td>
                    <?php if (!$delete): ?>
                        <td>
                            <input id="camera<?= $camera->id ?>" type="submit" onclick="edit(this)"
                                   value="Редактировать"
                                   class="btn">
                            <a href="/invent/delete/<?= $camera->id ?>" class="btn btn-danger">Удалить</a>
                        </td>
                    <?php endif; ?>
                    <td name="<?= $camera->name ?>" id="<?= $camera->name ?>"></td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="overlay" style="display:none;"></div>
    <div class="popup" style="display:none;">
        <img id="popup-image" src="/test.jpg" alt=""/>
        <label class="close" title="Закрыть"></label>
    </div>

<?php $this->startBlock('scripts'); ?>

    <script>
        var options = {
            valueNames: ['name', 'status', 'ip', 'model', 'place', 'location', 'id']
        };

        var userList = new List('cameras', options);</script>
    <script>
        var edit = function (e) {
            $('#editForm').css('display', 'inline');
            $('tr#' + e.id + ' td').each(function (i, elem) {
                name = elem.getAttribute("name");
                $("[name~='" + name + "']").val(elem.innerHTML);
            });
            $('input#urlAdmin').val($('#' + e.id + ' td a').attr('href'));
            window.scrollTo(0, 0);
        };

        var cancel = function (e) {
            $('#editForm').css('display', 'none');

            $('#forma div').children().each(function (i, elem) {
//                console.log(elem);

                name = elem.getAttribute("name");
                $(elem).val('');
            });
        }
    </script>
    <script>
        function sortTable(n) {
            var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
            table = document.getElementById("myTable");
            switching = true;
            //Set the sorting direction to ascending:
            dir = "asc";
            /*Make a loop that will continue until
             no switching has been done:*/
            while (switching) {
                //start by saying: no switching is done:
                switching = false;
                rows = table.getElementsByTagName("TR");
                /*Loop through all table rows (except the
                 first, which contains table headers):*/
                for (i = 1; i < (rows.length - 1); i++) {
                    //start by saying there should be no switching:
                    shouldSwitch = false;
                    /*Get the two elements you want to compare,
                     one from current row and one from the next:*/
                    x = rows[i].getElementsByTagName("TD")[n];
                    y = rows[i + 1].getElementsByTagName("TD")[n];
                    /*check if the two rows should switch place,
                     based on the direction, asc or desc:*/
                    if (dir == "asc") {
                        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                            //if so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    } else if (dir == "desc") {
                        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                            //if so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    }
                }
                if (shouldSwitch) {
                    /*If a switch has been marked, make the switch
                     and mark that a switch has been done:*/
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                    //Each time a switch is done, increase this count by 1:
                    switchcount++;
                } else {
                    /*If no switching has been done AND the direction is "asc",
                     set the direction to "desc" and run the while loop again.*/
                    if (switchcount == 0 && dir == "asc") {
                        dir = "desc";
                        switching = true;
                    }
                }
            }
        }
    </script>
    <!--    <script>-->
    <!--arr = JSON.parse('--><? //=json_encode($jsonCam)?><!--');-->
    <!--        console.log(arr);-->
    <!---->
    <!--        var ajaxPing = function (url, elemname) {-->
    <!--            var request = $.ajax({-->
    <!--                url: url-->
    <!--            });-->
    <!---->
    <!--            request.done(function( msg ) {-->
    <!--                $( "#"+elemname ).html( 'ok' );-->
    <!--            });-->
    <!---->
    <!--            request.fail(function( jqXHR, textStatus ) {-->
    <!--                $( "#"+elemname ).html( 'fail' );-->
    <!--            });-->
    <!--        };-->
    <!--        $.each(arr, function(index, value) {-->
    <!--            console.log(index+":"+value);-->
    <!--            ajaxPing(value, index);-->
    <!--        });-->
    <!--</script>-->


    <script>

        $('.zoom').on('click', function () {
            $('.overlay').fadeIn();
            $('.popup').fadeIn();
            $('#popup-image').attr('src', $(this).attr('src'));
        });

        $(".close, .overlay").click(function () {
            $(".popup").slideUp();
            $('.overlay').fadeOut();
        });
    </script>


<?php $this->endBlock(); ?>