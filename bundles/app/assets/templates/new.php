<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 14.03.2017
 * Time: 14:01
 */
$names = (json_decode(file_get_contents('http://192.168.22.239/videowall2/json.php')));
?>
<div id="editForm" style="display: none" >
	<form id="forma" role="form" class="form-inline" method="POST" action="/invent/new">
        <input name="id" id="id" type="text" class="form-control" hidden>
		<div class = "wrapper container">
			<div class="row">
				<div class="col-md-3 form-group">
					<label>Имя: </label>
<!--					<input type="text" name="name" id="place"-->
<!--						class="form-control">-->
                    <input type="text" name="name" id="place"
                           list="name_list"
                           class="form-control">
                    <datalist id="name_list">
                        <?php foreach ($names as $name): ?>
                            <option><?= str_replace(".jpg","",$name)?></option>
                        <?php endforeach; ?>
                    </datalist>
				</div>
				<div class="col-md-3 form-group">
					<label>Расположение: </label>
					<input name="place" id="place" type="text"
						class="form-control">
				</div>
				<div class="col-md-3 form-group">
					<label>Локальный IP: </label>
					<input type="text" name="ipLocal"
						class="form-control">
				</div>
				<div class="col-md-3 form-group">
					<label>URL админки: </label>
					<input type="text" name="urlAdmin" id="urlAdmin"
						class="form-control">
				</div>
		
			</div>
			<div class="row">
				<div class="col-md-3 form-group"><label>Участок: </label>
					<input type="text" name="area" id="area"
						list="area_list"
						class="form-control">
					<datalist id="area_list">
						<?php foreach ($areas as $area): ?>
							<option><?= $area->name ?></option>
						<?php endforeach; ?>
					</datalist>
				</div>
                <div class="col-md-3 form-group"><label>Модель: </label>
                    <input type="text" name="model" id="model"
                           list="model_list"
                           class="form-control">
                    <datalist id="model_list">
                        <?php foreach ($models as $model): ?>
                            <option><?= $model->name ?></option>
                        <?php endforeach; ?>
                    </datalist>
                </div>
				<div class="col-md-3 form-group">
					<label>Состояние: </label>
					<input type="text" name="status" id="status"
						list="status_list"
						class="form-control">
					<datalist id="status_list">
						<?php foreach ($statuses as $status): ?>
							<option><?= $status->name ?></option>
						<?php endforeach; ?>
					</datalist>
				</div>
				<div class="col-md-3 form-group"><label>Логин: </label><input type="text" name="login" class="form-control"></div>
				<div class="col-md-3 form-group"><label>Пароль: </label><input type="text" name="password" class="form-control"></div>
			</div>
			<div class="row">
				<div class="col-md-10"><label>Комментарий: </label><textarea name="comment"></textarea></div>
				<div class="col-md-2">
                    <a class="btn btn-danger" onclick="cancel(this)">Отменить</a>
					<button type="submit" class="btn btn-success">Отредактировать</button>
				</div>
			</div>
		</div>
	</form>
</div>